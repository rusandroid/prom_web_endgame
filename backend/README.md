# HOW-TO v1

## Thread management

### Create thread

POST /api/v1/thread/create

Request:
  * \[username\] user name. (!= null)
  * \[text\] main text of thread. (!= null)
  * \[image\] url to image.
  * \[link\] id of the original post.
  * ~~\[replies\] comments of the thread.~~ Not implemented yet.

### Delete thread

POST /api/v1/thread/delete

Request:
  * \[id\] Post id. (!= null)

---

## Thread usage

### Get full thread with replies

GET /api/v1/thread/get/{id}

Return:
  * \[root\] : Array -- thread root
  * \[replies\] : Array -- thread replies

### Get page of threads

GET /api/v1/thread/getlist/{page}

### Get only one thread or reply

GET /api/v1/thread/getraw/{id}

### Get page of newest threads and replies

GET /api/v1/thread/getlistraw/{page}