<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    // См. файл миграции
    protected $casts = [
        'replies' => 'array'
    ];
    
    protected $attributes = array(
	'username' => 'anon',
	'link' => -1,
	'image' => null,
	'replies' => "[]",
    );
    protected $fillable = array(
	'username',
	'text',
	'link',
	'image'
    );
    use HasFactory;
}
