<?php

namespace App\Http\Controllers;

use App\Models\Thread;
use Illuminate\Http\Request;

class ThreadController extends Controller
{
    public function create(Request $request)
    {
	$request->validate([
	    'username' => 'required',
	    'text' => 'required'
	]);
        $thread = Thread::create($request->all());
	// TODO: Добавить добавление в список ответов
        
	$thread->save();
        return 200;
    }

    public function delete(Request $request)
    {
        $request->validate([
	    'id' => 'required'
	]);
        Thread::destroy($request->id);
        return 200;
    }

    public function getraw($id)
    {
        $res = Thread::find($id);
        if (!$res) {
            return 404;
        }
        return $res->first();
    }

    public function getlistraw($page)
    {
        $res = Thread::find((($page-1)*10)+1);
        if (!$res) {
            return 404;
        }
        return $res->take(10)->get();
    }
    
    public function get($id) 
    {
        $original = Thread::where('id', $id)->first();
	
        if (!$original) {
            return 404;
        }
	
	$original = $original->toArray();
        
        $replies = Thread::where('link', $original['id'])
                ->orderBy("created_at")
                ->get()
                ->toArray();
        
        $res = (array("root" => $original, "replies" => $replies));
        
        return $res;
    }
    
    public function getlist($page) 
    {
        $res = Thread::where("link", -1)
		->orderBy("created_at")
		->take(10)
		->get()
		->toArray();
	return $res;
    }

}
