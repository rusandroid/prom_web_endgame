<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use App\Models\Thread;
use App\Http\Controllers\ThreadController;

//use App\Models\User;
//use Illuminate\Support\Facades\Hash;
//use Illuminate\Validation\ValidationException;
//use Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful;

Route::prefix("v1")->group(function () 
{    
    Route::post("/thread/create", 
            [ThreadController::class, 'create'])->name("thread-create");

    // TODO: Add auth
    Route::post("/thread/delete", 
            [ThreadController::class, 'delete'])->name("thread-delete");

    Route::get("/thread/getraw/{id}", 
            [ThreadController::class, 'getraw'])->name("thread-get-raw");
    Route::get("/thread/getlistraw/{page}", 
            [ThreadController::class, 'getlistraw'])->name("thread-getlist-raw");

    Route::get("/thread/get/{id}", 
            [ThreadController::class, 'get'])->name("thread-get");
    Route::get("/thread/getlist/{page}", 
            [ThreadController::class, 'getlist'])->name("thread-getlist");

});
