<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('threads', function (Blueprint $table) {
            $table->id();
            
            $table->string("username", 50)->default("anon");
            // Основной текст треда
            $table->string("text", 500);
            // URL к изображению
            $table->string("image", 255)->nullable();
            // ССылка на ответ (Или Null/отр. цисла, если это корневой тред)
            // Использовать id треда
            $table->integer("link")->default(-1);
            /* Это ссылки ответов на тред. 
             *
             * Т.к. модели в Laravel не могут быть массивами, я использую
             * этот способ:
             * https://stackoverflow.com/a/32955501
             */
            $table->json("replies")->default("[]");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('threads');
    }
}
